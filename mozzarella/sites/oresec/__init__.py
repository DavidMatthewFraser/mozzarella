from tg import lurl


title = "OreSec"
logo_url = lurl('/img/full.jpeg')
default_theme = 'dark'
navbar = (
    dict(id='wiki', href='https://oresec.mines.edu/wiki', display_name='Wiki'),
)
