from tg import lurl

from .covid import COVIDController


title = 'Mines LUG'
logo_url = lurl('/img/logo_notext.svg')
logo_html = 'mozzarella.sites.lug.templates.logo'


controllers = {
    'covid': COVIDController(),
}


navbar = (
    dict(id='helpsessions', href=lurl('/covid'), display_name='COVID'),
    dict(id='mirrors', href=lurl('https://lug.mines.edu/mirrors/'), display_name='Mirrors'),
)
