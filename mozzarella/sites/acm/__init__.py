from tg import lurl

from .research import ResearchController


title = "Mines ACM"
logo_url = lurl('/img/full.svg')


controllers = dict(
    research=ResearchController(),
)

navbar = (
    dict(id='research', href=lurl('/research'), display_name='Research'),
)
