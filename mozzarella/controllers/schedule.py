"""
Schedule page and iCalendar controller
"""
import datetime
import pytz

from icalendar import Calendar, Event
from tg import expose, response, request, config

from mozzarella.lib.base import BaseController
from mozzarella.model import DBSession, Meeting


class ScheduleController(BaseController):
    def __init__(self):
        self.meetings = DBSession.query(Meeting)

    @expose('mozzarella.templates.schedule')
    def index(self):
        """
        Handle the schedule page. If the request is for the .ics file, it will
        return the schedule in iCal format.
        """
        # Filter meetings that occurred in the past
        upcoming_meetings = self.meetings.order_by(Meeting.date.asc()).filter(
            Meeting.date > datetime.datetime.now() - datetime.timedelta(hours=3)
        ).all()

        previous_meetings = self.meetings.order_by(Meeting.date.desc()).filter(
            Meeting.date < datetime.datetime.now() - datetime.timedelta(hours=3)
        ).all()

        return dict(page='schedule', meetings=upcoming_meetings, prev_meetings=previous_meetings)

    @expose(content_type='text/calendar')
    def ical(self):
        """ Returns the iCalendar version of the schedule """
        cal = Calendar()
        cal.add('prodid', config.get('meetings.icalendar.prodid'))
        cal.add('version', '2.0')
        if config.get('meetings.icalendar.name'):
            cal.add('X-WR-CALNAME', config['meetings.icalendar.name'])
        if config.get('meetings.icalendar.description'):
            cal.add('X-WR-CALDESC', config['meetings.icalendar.description'])

        for m in self.meetings.all():
            event = Event()
            event.add('summary', m.title)
            event.add('description', m.description)
            event.add('location', m.location)
            d = m.date.replace(tzinfo=pytz.timezone(config.get('meetings.timezone')))
            event.add('dtstart', d)
            event.add('dtend', d + m.duration)
            event.add('dtstamp', d)

            cal.add_component(event)

        return cal.to_ical()
