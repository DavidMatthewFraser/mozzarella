"""Research controller module"""

from tg import expose

from mozzarella.lib.base import BaseController


class MatrixController(BaseController):
    """Controller for matrix page"""

    @expose('mozzarella.templates.matrix')
    def index(self):
        return dict(page='matrix')
