"""
Increare MAX presentation title

Revision ID: b40dcb480acf
Revises: aeeb5f0ffb69
Create Date: 2019-01-17 14:42:35.017022

"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'b40dcb480acf'
down_revision = 'aeeb5f0ffb69'


def upgrade():
    # this does not work with SQLite
    op.alter_column('presentation', 'title',
                    type_=sa.types.String(128),
                    existing_type=sa.types.String(32))


def downgrade():
    # this does not work with SQLite
    op.alter_column('presentation', 'title',
                    type_=sa.types.String(32),
                    existing_type=sa.types.String(128))
