"""
Survey description

Revision ID: e0f30c4c92e9
Revises: d005e978789f
Create Date: 2020-02-06 13:16:42.594153

"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'e0f30c4c92e9'
down_revision = 'd005e978789f'


def upgrade():
    op.add_column('survey', sa.Column('description', sa.Unicode(), nullable=True))


def downgrade():
    op.drop_column('survey', 'description')
