"""
Meeting more info

Revision ID: d005e978789f
Revises: d615989a2933
Create Date: 2020-01-17 20:46:34.360779

"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'd005e978789f'
down_revision = 'd615989a2933'


def upgrade():
    op.add_column('meeting', sa.Column('more_info', sa.Unicode(), nullable=True))


def downgrade():
    op.drop_column('meeting', 'more_info')
