Listreg
==========

Listreg is a Python script used to continuously register people to a club's
mailing list (e.g. at a club fair or similar).

Usage
------------------------------------

This assumes a properly-deployed Mozzarella instance.

Run the listreg script from the command line with ``python3``. It takes one
mandatory and one optional argument. The first argument is the mailing list's
"subscribe" url, e.g. ``https://lug.mines.edu/mailinglist/subscribe``. The
second, optional argument is a file containing an ASCII art "banner" for the
club. Two examples are provided: ``lugreg.txt`` and ``acmreg.txt``.

A full example run would be:

   .. code:: console
      
      $ python3 listreg.py https://lug.mines.edu/mailinglist/subscribe lugreg.txt
